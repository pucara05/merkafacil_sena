/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package clases;

/**
 *
 * @author USUARIO
 */
public class Clase_detalle {
	private long id_producto;
	private int cantida_detalle;
	private double sub_total_detalle;

	public long getId_producto() {
		return id_producto;
	}

	public void setId_producto(long id_producto) {
		this.id_producto = id_producto;
	}

	public int getCantida_detalle() {
		return cantida_detalle;
	}

	public void setCantida_detalle(int cantida_detalle) {
		this.cantida_detalle = cantida_detalle;
	}

	public double getSub_total_detalle() {
		return sub_total_detalle;
	}

	public void setSub_total_detalle(double sub_total_detalle) {
		this.sub_total_detalle = sub_total_detalle;
	}

	public Clase_detalle(long id_producto, int cantida_detalle, double sub_total_detalle) {
		this.id_producto = id_producto;
		this.cantida_detalle = cantida_detalle;
		this.sub_total_detalle = sub_total_detalle;
	}
}
