/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package clases;

import java.util.Date;

/**
 *
 * @author USUARIO
 */
public class Clase_pedido {
	private long id_pedido;
	private Date fecha_pedido;

	public long getId_pedido() {
		return id_pedido;
	}

	public void setId_pedido(long id_pedido) {
		this.id_pedido = id_pedido;
	}

	public Date getFecha_pedido() {
		return fecha_pedido;
	}

	public void setFecha_pedido(Date fecha_pedido) {
		this.fecha_pedido = fecha_pedido;
	}

	public long getCedula_cliente() {
		return cedula_cliente;
	}

	public void setCedula_cliente(long cedula_cliente) {
		this.cedula_cliente = cedula_cliente;
	}

	public double getValor_total_pedido() {
		return valor_total_pedido;
	}

	public void setValor_total_pedido(double valor_total_pedido) {
		this.valor_total_pedido = valor_total_pedido;
	}

	public long getCedula_empleado() {
		return cedula_empleado;
	}

	public void setCedula_empleado(long cedula_empleado) {
		this.cedula_empleado = cedula_empleado;
	}
	private long cedula_cliente;
	private double valor_total_pedido;
	private long cedula_empleado;

	public Clase_pedido(long id_pedido, Date fecha_pedido, long cedula_cliente, double valor_total_pedido, long cedula_empleado) {
		this.id_pedido = id_pedido;
		this.fecha_pedido = fecha_pedido;
		this.cedula_cliente = cedula_cliente;
		this.valor_total_pedido = valor_total_pedido;
		this.cedula_empleado = cedula_empleado;
	}
	
	
}
