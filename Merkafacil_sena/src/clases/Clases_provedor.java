/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package clases;

/**
 *
 * @author USUARIO
 */
public class Clases_provedor {
	private long nit_provedor;
	private String nombre_provedor;

	public long getNit_provedor() {
		return nit_provedor;
	}

	public void setNit_provedor(long nit_provedor) {
		this.nit_provedor = nit_provedor;
	}

	public String getNombre_provedor() {
		return nombre_provedor;
	}

	public void setNombre_provedor(String nombre_provedor) {
		this.nombre_provedor = nombre_provedor;
	}

	public String getDirecion_provedor() {
		return direcion_provedor;
	}

	public void setDirecion_provedor(String direcion_provedor) {
		this.direcion_provedor = direcion_provedor;
	}

	public String getEmail_provedor() {
		return email_provedor;
	}

	public void setEmail_provedor(String email_provedor) {
		this.email_provedor = email_provedor;
	}

	public long getTelefono_provedor() {
		return telefono_provedor;
	}

	public void setTelefono_provedor(long telefono_provedor) {
		this.telefono_provedor = telefono_provedor;
	}

	public String getContacto_provedor() {
		return contacto_provedor;
	}

	public void setContacto_provedor(String contacto_provedor) {
		this.contacto_provedor = contacto_provedor;
	}
	private String direcion_provedor;

	public Clases_provedor(long nit_provedor, String nombre_provedor, String direcion_provedor, String email_provedor, long telefono_provedor, String contacto_provedor) {
		this.nit_provedor = nit_provedor;
		this.nombre_provedor = nombre_provedor;
		this.direcion_provedor = direcion_provedor;
		this.email_provedor = email_provedor;
		this.telefono_provedor = telefono_provedor;
		this.contacto_provedor = contacto_provedor;
	}
	private String email_provedor;
	private long telefono_provedor;
	private String contacto_provedor;
	
	
}
