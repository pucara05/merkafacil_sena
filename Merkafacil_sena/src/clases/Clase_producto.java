/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package clases;

/**
 *
 * @author USUARIO
 */
public class Clase_producto {
private long id_producto;

	public long getId_producto() {
		return id_producto;
	}

	public void setId_producto(long id_producto) {
		this.id_producto = id_producto;
	}

	public String getNombre_producto() {
		return nombre_producto;
	}

	public void setNombre_producto(String nombre_producto) {
		this.nombre_producto = nombre_producto;
	}

	public double getValor_producto() {
		return valor_producto;
	}

	public void setValor_producto(double valor_producto) {
		this.valor_producto = valor_producto;
	}

	public long getStock_minimo() {
		return stock_minimo;
	}

	public void setStock_minimo(long stock_minimo) {
		this.stock_minimo = stock_minimo;
	}

	public long getExistencia_producto() {
		return existencia_producto;
	}

	public void setExistencia_producto(long existencia_producto) {
		this.existencia_producto = existencia_producto;
	}

	public long getId_familiaP() {
		return id_familiaP;
	}

	public void setId_familiaP(long id_familiaP) {
		this.id_familiaP = id_familiaP;
	}

	public long getNit_provedor() {
		return nit_provedor;
	}

	public void setNit_provedor(long nit_provedor) {
		this.nit_provedor = nit_provedor;
	}
private String nombre_producto;

	public Clase_producto(long id_producto, String nombre_producto, double valor_producto, long stock_minimo, long existencia_producto, long id_familiaP, long nit_provedor) {
		this.id_producto = id_producto;
		this.nombre_producto = nombre_producto;
		this.valor_producto = valor_producto;
		this.stock_minimo = stock_minimo;
		this.existencia_producto = existencia_producto;
		this.id_familiaP = id_familiaP;
		this.nit_provedor = nit_provedor;
	}
private double valor_producto;
private long stock_minimo;
private long existencia_producto;
private long id_familiaP;
private long nit_provedor;
	
}
