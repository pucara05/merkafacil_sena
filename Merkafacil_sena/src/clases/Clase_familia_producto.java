/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package clases;

/**
 *
 * @author USUARIO
 */
public class Clase_familia_producto {
private long id_familiaP;
private String nombre_familiaP;

	public long getId_familiaP() {
		return id_familiaP;
	}

	public void setId_familiaP(long id_familiaP) {
		this.id_familiaP = id_familiaP;
	}

	public String getNombre_familiaP() {
		return nombre_familiaP;
	}

	public void setNombre_familiaP(String nombre_familiaP) {
		this.nombre_familiaP = nombre_familiaP;
	}

	public Clase_familia_producto(long id_familiaP, String nombre_familiaP) {
		this.id_familiaP = id_familiaP;
		this.nombre_familiaP = nombre_familiaP;
	}
	
}
