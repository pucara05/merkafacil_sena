/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package clases;
import java.util.Date;
/**
 *
 * @author USUARIO
 */
public class Clase_empleado {
	private long cedula_empleado;
	private String nombre_empleado;
	private String apellido_empleado;

	public long getCedula_empleado() {
		return cedula_empleado;
	}

	public void setCedula_empleado(long cedula_empleado) {
		this.cedula_empleado = cedula_empleado;
	}

	public String getNombre_empleado() {
		return nombre_empleado;
	}

	public void setNombre_empleado(String nombre_empleado) {
		this.nombre_empleado = nombre_empleado;
	}

	public String getApellido_empleado() {
		return apellido_empleado;
	}

	public void setApellido_empleado(String apellido_empleado) {
		this.apellido_empleado = apellido_empleado;
	}

	public String getDireccion_empleado() {
		return direccion_empleado;
	}

	public void setDireccion_empleado(String direccion_empleado) {
		this.direccion_empleado = direccion_empleado;
	}

	public long getTelefono_empleado() {
		return telefono_empleado;
	}

	public void setTelefono_empleado(long telefono_empleado) {
		this.telefono_empleado = telefono_empleado;
	}

	public String getEmail_empleado() {
		return email_empleado;
	}

	public void setEmail_empleado(String email_empleado) {
		this.email_empleado = email_empleado;
	}

	public Date getFecha_ingreso_empleado() {
		return fecha_ingreso_empleado;
	}

	public void setFecha_ingreso_empleado(Date fecha_ingreso_empleado) {
		this.fecha_ingreso_empleado = fecha_ingreso_empleado;
	}

	public Clase_empleado(long cedula_empleado, String nombre_empleado, String apellido_empleado, String direccion_empleado, long telefono_empleado, String email_empleado, Date fecha_ingreso_empleado) {
		this.cedula_empleado = cedula_empleado;
		this.nombre_empleado = nombre_empleado;
		this.apellido_empleado = apellido_empleado;
		this.direccion_empleado = direccion_empleado;
		this.telefono_empleado = telefono_empleado;
		this.email_empleado = email_empleado;
		this.fecha_ingreso_empleado = fecha_ingreso_empleado;
	}
	private String direccion_empleado;
	private long telefono_empleado;
	private String email_empleado;
	private Date fecha_ingreso_empleado;
	
	
}
