/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package clases;

/**
 *
 * @author USUARIO
 */
public class Clase_cliente {
	private long cedula_cliente;
	private String nombre_cliente;

	public long getCedula_cliente() {
		return cedula_cliente;
	}

	public void setCedula_cliente(long cedula_cliente) {
		this.cedula_cliente = cedula_cliente;
	}

	public String getNombre_cliente() {
		return nombre_cliente;
	}

	public void setNombre_cliente(String nombre_cliente) {
		this.nombre_cliente = nombre_cliente;
	}

	public String getApellido_cliente() {
		return apellido_cliente;
	}

	public void setApellido_cliente(String apellido_cliente) {
		this.apellido_cliente = apellido_cliente;
	}

	public String getDirecion_cliente() {
		return direcion_cliente;
	}

	public void setDirecion_cliente(String direcion_cliente) {
		this.direcion_cliente = direcion_cliente;
	}

	public long getTelefono_cliente() {
		return telefono_cliente;
	}

	public void setTelefono_cliente(long telefono_cliente) {
		this.telefono_cliente = telefono_cliente;
	}

	public String getEmail_cliente() {
		return email_cliente;
	}

	public void setEmail_cliente(String email_cliente) {
		this.email_cliente = email_cliente;
	}

	public Clase_cliente(long cedula_cliente, String nombre_cliente, String apellido_cliente, String direcion_cliente, long telefono_cliente, String email_cliente) {
		this.cedula_cliente = cedula_cliente;
		this.nombre_cliente = nombre_cliente;
		this.apellido_cliente = apellido_cliente;
		this.direcion_cliente = direcion_cliente;
		this.telefono_cliente = telefono_cliente;
		this.email_cliente = email_cliente;
	}
	private String apellido_cliente;
	private String direcion_cliente;
	private long telefono_cliente;
	private String email_cliente;
	
}
