/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package archivo;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author USUARIO
 */
public class Archivo {
	public static void generar_archivo(String nombre_archivo, String cadena){
		
		try {
			File archivo= new File(nombre_archivo);
			FileWriter  escritura= new FileWriter(archivo);
			escritura.write(cadena);
			escritura.close();
		} catch (IOException ex) {
			Logger.getLogger(Archivo.class.getName()).log(Level.SEVERE, null, ex);
		}
		
	}
	
}
